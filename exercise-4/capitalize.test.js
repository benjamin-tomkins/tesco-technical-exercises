const capitalize = require('./capitalize');

describe('capitalize', () => {

    test('accepts a string and returns an capitalized string', () => {
        const result = capitalize('word');
        expect(result).toEqual('Word');
    });

        test('accepts a string and returns an array containing a string', () => {
        const result = capitalize('word');
        expect(result).toEqual('Word');
    });

    test('accepts an array with a string and returns an array with a string', () => {
        const result = capitalize(['word', 'list']);
        expect(result).toEqual(['Word', 'List']);
    });
});