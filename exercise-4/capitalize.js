/**
 * Candidate name: Benjamin M Tomkins
 * Task : Create a function that will accept one parameter - an array of strings or a string and return array of strings or a string with the words’ first letter capitalized
 * @function capitalize()
 * @param {array/string}
 * @return {array/string}
 */

const capitalize = (a) => {
    if ( typeof(a) == 'string') return a[0].toUpperCase() + a.slice(1);
    return a.map(a => a[0].toUpperCase() + a.slice(1));
};

module.exports = capitalize;