#### Sample TDD code for the Tesco technical exercises
#####Candidate: Benjamin M Tomkins

`git clone https://benjamin-tomkins@bitbucket.org/benjamin-tomkins/tesco-technical-exercises.git`

`npm test`

- The first 4 questions have been answered due to time constraints
- Code samples have been updated to ES6 syntax
- All completed code has been Red-Green TDD'd
- It should be noted that exercise 3 has some comments regarding a typo in the original question

