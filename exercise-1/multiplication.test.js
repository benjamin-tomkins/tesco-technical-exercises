const multi = require("./multiplication");

describe('multi', () => {
    test('invokes a multiplication function with arguments 3 and 4', () => {
        const result = multi(3, 4);
        expect(result).toBe(12);
    });
});