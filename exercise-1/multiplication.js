/**
 * Candidate name: Benjamin M Tomkins
 * Task: Create a function that will accept two parameters and return the multiplication of those
 * @function multi()
 * @param {int} a
 * @param {int} b
 * @return {int}
 */

const multi = (a, b) => a * b;

module.exports = multi;