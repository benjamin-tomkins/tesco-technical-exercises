/**
 * Candidate name: Benjamin M Tomkins
 * Task : Create a function that will take one parameter as a string and return if it is a localhost IP of a pattern 127.x.x.x
 * @function isLocalhostIP()
 * @param {string} a
 * @return { boolean }
 */

const isLocalhostIP = (a) => {
    // INCOMPLETE BECAUSE OF TIME CONSTRAINTS
};

module.exports = isLocalhostIP;