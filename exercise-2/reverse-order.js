/**
 * Candidate name: Benjamin M Tomkins
 * Task : Create a function that will accept one parameter - a string and return the same string in reverse order
 * @function reverse()
 * @param {string}
 * @return {string}
 */

const reverse = (a) => [...a].reverse().join('');

module.exports = reverse;