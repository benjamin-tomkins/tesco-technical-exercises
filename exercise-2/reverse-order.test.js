const reverse = require("./reverse-order");

describe('reverse', () => {
    test('returns a string in reverse order', () => {
        const result = reverse('a_string');
        expect(result).toBe('gnirts_a');
    });
});