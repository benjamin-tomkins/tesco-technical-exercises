const { fibonacci, nthFibNumber } = require("./fibonacci");

describe('nthFibNumber', () => {
    test('should return 0 as the first fibonacci number', () => {
        expect(nthFibNumber(1)).toBe(0);
    });

    test('should return 1 as the 2nd fibonacci number', () => {
        expect(nthFibNumber(2)).toBe(1);
    });

    test('should return 1 as the 3rd fibonacci number', () => {
        expect(nthFibNumber(3)).toBe(1);
    });

    test('should return the 10th fibonacci number', () => {
        expect(nthFibNumber(10)).toBe(34);
    });

    test('should throw error if n <= 0', () => {
        expect(() => nthFibNumber(0)).toThrowError('n cannot be 0 or negative');
        expect(() => nthFibNumber(-1)).toThrowError('n cannot be 0 or negative');
    });
});

describe('fibonacci', () => {
    test('should return the first 10 fibonacci numbers in an array', () => {
        expect(fibonacci([1,1])).toEqual([1,1,2,3,5,8,13,21,34,55]);
    });
});