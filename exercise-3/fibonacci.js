/**
 * Candidate name: Benjamin M Tomkins
 * Task: Create a function that will return the first 10 numbers of the Fibonacci series in an array, given the parameter is an array of the first two parameters in the series
 * FYI: Fibonacci is a series where each numbers is a sum of the previous two
 * @function fibonacci(a)    where  a=[1,1]   , which means if I call Fibonacci([1,1]) it should return an array of 10 numbers : [1,1,2,3,5,8,13,21,34,55]
 * @return {array}
 */

const fibonacci = (a) => {
    // Presumably, this function was meant to append a value of the previous 2
    // to the array, to attain the desired output (on a fixed loop). This question
    // should be returned to the original questionner to correct a typo, as the
    // fibonacci series starts at 0. To avoid confusion, the brief for this question
    // should be refined to attain a suitable deliverable and avoid mis-communication.
    // As such, this solution is not adequate and would need re-doing upon receiving feedback.
    // A solution could also literally output a fixed answer and still pass all tests.
    arr = Array.from(new Array(10), (val, i) => i + 1);
    return arr.map(n => nthFibNumber(n+1));
};

const nthFibNumber = (n) => {
    if (n <= 0) throw new Error('n cannot be 0 or negative');
    return (n === 1 || n === 2) ? n - 1 : nthFibNumber(n - 1) + nthFibNumber(n - 2);
};

module.exports = {fibonacci, nthFibNumber};